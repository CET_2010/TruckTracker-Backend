package com.truck.service;

import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import com.truck.bean.DistributorBean;
import com.truck.bean.ReportCardBean;
import com.truck.bean.TripBean;
import com.truck.bean.TripCountBean;
import com.truck.bean.TripStatus;
import com.truck.bean.TruckBean;
import com.truck.bean.UserBean;
import com.truck.util.PropertyUtil;

public class Orm {

  private OrmUtils ormUtils = new OrmUtils();
  private String dbName;

  public Orm() {
    this.dbName = new PropertyUtil().getProperty("DB_NAME");
  }

  public String getEncodedString(String plainString) {
    try {
      if ((plainString != null) || (plainString != ""))
        return Base64.getEncoder().encodeToString(plainString.getBytes("utf-8"));
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return "";
  }

  public String addUser(UserBean user) {
    dbName = "truckdb";
    String sql = "{ call " + dbName + ".sp_createUser(?,?,?,?,?,?,?) }";
    String password = getEncodedString(user.getPassword());
    String response = "";
    try (Connection conn = ormUtils.getConnection(); CallableStatement stmt = conn.prepareCall(sql)) {
      stmt.setString(1, user.getUsername().toUpperCase());
      stmt.setString(2, password);
      String role = user.getUserRole();
      if ((role == null) || (role.equals("")) || (role.length() == 0))
        role = "USER";
      stmt.setString(3, role.toUpperCase());
      if (user.getDistributorId() == 0)
        stmt.setString(4, null);
      else
        stmt.setInt(4, user.getDistributorId());
      if (user.getUserId() == 0)
        stmt.setString(5, null);
      else
        stmt.setInt(5, user.getUserId());
      stmt.setInt(6, user.getLocation());
      stmt.registerOutParameter(7, java.sql.Types.VARCHAR);
      System.out.println("addUser Query ----" + stmt);
      stmt.execute();
      response = stmt.getString(7);
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return response;
  }

  public String addTruck(TruckBean truck) {

    String sql = "{ call " + dbName + ".sp_createTruck(?,?,?,?,?,?,?,?,?,?) }";
    String response = null;
    System.out.println(sql);
    try (Connection conn = ormUtils.getConnection(); CallableStatement stmt = conn.prepareCall(sql)) {
      stmt.setString(1, truck.getTruckName());
      stmt.setString(2, truck.getDriverName());
      stmt.setString(3, truck.getDriverNumber());
      stmt.setString(4, truck.getDriverAltNumber());
      stmt.setString(5, truck.getDriverLicense());
      stmt.setString(6, truck.getCarrierCode());
      stmt.setString(7, truck.getCarrierName());
      if (truck.getUserId() == 0)
        stmt.setString(8, null);
      else
        stmt.setInt(8, truck.getUserId());
      stmt.setInt(9, truck.getLocation());
      stmt.registerOutParameter(10, java.sql.Types.VARCHAR);
      System.out.println("addTruck Query ----" + stmt);
      stmt.execute();
      response = stmt.getString(10);
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return response;
  }

  public String addDistributor(DistributorBean distributor) {
    String sql = "{ call " + dbName + ".sp_createDistributor(?,?,?,?,?,?,?,?) }";
    String response = "";
    try (Connection conn = ormUtils.getConnection(); CallableStatement stmt = conn.prepareCall(sql)) {
      stmt.setString(1, distributor.getName());
      stmt.setString(2, distributor.getSapCode());
      stmt.setInt(3, distributor.getRtkm());
      stmt.setString(4, distributor.getDistributorNumber());
      stmt.setString(5, distributor.getGodownNumber());
      if (distributor.getUserId() == 0)
        stmt.setString(6, null);
      else
        stmt.setInt(6, distributor.getUserId());
      stmt.setInt(7, distributor.getLocation());
      stmt.registerOutParameter(8, java.sql.Types.VARCHAR);
      System.out.println("addDistributor Query ----" + stmt);
      stmt.execute();
      response = stmt.getString(8);
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return response;
  }

  public String addTrip(TripBean trip) {
    String sql = "{ call " + dbName + ".sp_createTrip(?,?,?,?) }";
    String response = "";
    try (Connection conn = ormUtils.getConnection(); CallableStatement stmt = conn.prepareCall(sql)) {
      stmt.setString(1, trip.getTruckName());
      if (trip.getUserId() == 0)
        stmt.setString(2, null);
      else
        stmt.setInt(2, trip.getUserId());
      stmt.setInt(3, trip.getLocation());
      stmt.registerOutParameter(4, java.sql.Types.VARCHAR);
      System.out.println("addTrip Query ----" + stmt);
      stmt.execute();
      response = stmt.getString(4);
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return response;
  }

  public UserBean getUser(UserBean user) {
    String sql = "SELECT ID,NAME,ROLE,DISTRIBUTOR_ID,LOCATION_ID from " + dbName
        + ".USER_LOGIN WHERE NAME=? AND PASSWORD=? AND IS_DELETED='N'";
    String password = getEncodedString(user.getPassword());
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setString(1, user.getUsername().toUpperCase());
      stmt.setString(2, password);
      System.out.println("getUser Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet.first()) {
        user.setId(resultSet.getInt(1));
        user.setUserRole(resultSet.getString(3));
        user.setDistributorId(resultSet.getInt(4));
        user.setLocation(resultSet.getInt(5));
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return user;
  }

  public TripCountBean getTripCount(String locationId, String distributorId) {
    StringBuilder sql = new StringBuilder("SELECT COUNT(*),STATUS FROM TRIP WHERE LOCATION_ID=? ");
    if (distributorId != null && distributorId.trim().length() > 0 && !distributorId.equals("0")) {
      sql.append("AND END_GATE_ID =? ");
    }
    sql.append(" AND IS_DELETED ='N' GROUP BY STATUS"); 
    TripCountBean countBean = new TripCountBean();
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql.toString())) {
      stmt.setString(1, locationId);
      if (distributorId != null && distributorId.trim().length() > 0 && !distributorId.equals("0")) {
        stmt.setString(2, distributorId);
      }
      System.out.println("getTripCount Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();     
      while (resultSet.next()) {
        if (resultSet.getString(2).equals(TripStatus.PARKING.toString())) {
          countBean.setPark_count(resultSet.getInt(1));
        } else if (resultSet.getString(2).equals(TripStatus.LOADING.toString())) {
          countBean.setLoad_count(resultSet.getInt(1));
        } else if (resultSet.getString(2).equals(TripStatus.ENROUTE.toString())) {
          countBean.setEnroute_count(resultSet.getInt(1));
        } else if (resultSet.getString(2).equals(TripStatus.UNLOADING.toString())) {
          countBean.setUnload_count(resultSet.getInt(1));
        } else if (resultSet.getString(2).equals(TripStatus.COMPLETED.toString())) {
          countBean.setComplete_count(resultSet.getInt(1));
        }
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }System.out.println("TripCountBean::"+countBean);
    return countBean;
  }

  public List<UserBean> getUserList(String userName, String locationId) {
    if (isEmpty(locationId)) {
      return null;
    }
    List<UserBean> userList = new ArrayList<UserBean>();
    StringBuilder sql = null;
    sql = new StringBuilder("SELECT ID,NAME,ROLE,DISTRIBUTOR_ID,LOCATION_ID from " + dbName
        + ".USER_LOGIN WHERE NAME LIKE '%");
    sql.append(userName);
    sql.append("%' AND LOCATION_ID=? AND IS_DELETED='N' ORDER BY MODIFIED_ON DESC LIMIT 20");
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql.toString())) {
      stmt.setString(1, locationId);
      System.out.println("getUserList Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        UserBean user = new UserBean();
        user.setId(resultSet.getInt(1));
        user.setUsername(resultSet.getString(2));
        user.setUserRole(resultSet.getString(3));
        user.setDistributorId(resultSet.getInt(4));
        user.setLocation(resultSet.getInt(5));
        userList.add(user);
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return userList;
  }
  
  public List<TruckBean> getTruckList(String truckName, String distributorId, String locationId) {
    if (isEmpty(locationId)) {
      return null;
    }

    List<TruckBean> truckList = new ArrayList<TruckBean>();
    StringBuilder sql = null;

    if (distributorId != null && distributorId.trim().length() > 0 && !distributorId.equals("0")) {
      sql = new StringBuilder(
          "SELECT NAME,DRIVER_NAME,DRIVER_NUMBER,DRIVER_ALT_NUMBER,DRIVER_LICENSE,ID,LOCATION_ID," +
          "CARRIER_CODE,CARRIER_NAME FROM " + dbName
              + ".TRUCK WHERE IS_DELETED='N' AND ID IN (SELECT TRUCK_ID FROM " + dbName
              + ".TRIP WHERE LOCATION_ID= ? AND END_GATE_ID='" + distributorId + "')");
    } else {
      sql = new StringBuilder(
          "SELECT NAME,DRIVER_NAME,DRIVER_NUMBER,DRIVER_ALT_NUMBER,DRIVER_LICENSE,ID,LOCATION_ID,CARRIER_CODE" + 
          ",CARRIER_NAME from " + dbName
              + ".TRUCK WHERE LOCATION_ID= ? AND IS_DELETED='N'");
    }
    if (truckName != null && truckName.trim().length() > 0) {
      sql.append(" AND NAME LIKE '%").append(truckName).append("%'");
    }
    sql.append(" ORDER BY MODIFIED_ON DESC LIMIT 20");
    /*
     * if(userId!=null && userId.trim().length()>0){ sql += " AND CREATED_BY='" + userId + "'"; }
     */ System.out.println(sql);
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql.toString())) {
      stmt.setString(1, locationId);
      System.out.println("getTruckList Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        TruckBean truck = new TruckBean();
        truck.setTruckName(resultSet.getString(1));
        truck.setDriverName(resultSet.getString(2));
        truck.setDriverNumber(resultSet.getString(3));
        truck.setDriverAltNumber(resultSet.getString(4));
        truck.setDriverLicense(resultSet.getString(5));
        truck.setId(resultSet.getInt(6));
        truck.setLocation(resultSet.getInt(7));
        truck.setCarrierCode(resultSet.getString(8));
        truck.setCarrierName(resultSet.getString(9));
        truckList.add(truck);
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return truckList;
  }

  public List<TripBean> getTripList(String tripName, String distributorId, String locationId, String tripStatus) {
    if (isEmpty(locationId)) {
      return null;
    }
    List<TripBean> tripList = new ArrayList<TripBean>();
    StringBuilder sql = new StringBuilder(
        "SELECT TP.ID,TP.NAME,TK.NAME,TK.DRIVER_NAME,TK.DRIVER_LICENSE,TP.START_GATE,DIST.NAME,TP.START_GATE_ENTRY_TIME,"
            + "TP.START_GATE_EXIT_TIME,TP.END_GATE_ENTRY_TIME,TP.END_GATE_EXIT_TIME,TP.STATUS,TP.PARKING_ENTRY_TIME,TP.LOCATION_ID "
            + " FROM " + dbName + ".TRUCK TK," + dbName + ".TRIP TP" + " LEFT JOIN " + dbName
            + ".DISTRIBUTOR DIST ON TP.END_GATE_ID = DIST.ID"
            + " WHERE TP.TRUCK_ID=TK.ID AND TP.IS_DELETED='N' AND TP.LOCATION_ID=?");
    if (tripName != null && tripName.trim().length() > 0) {
      sql.append(" AND TP.NAME LIKE '%").append(tripName).append("%'");
    }
    if (distributorId != null && distributorId.trim().length() > 0 && !distributorId.equals("0")) {
      sql.append(" AND TP.END_GATE_ID='").append(distributorId).append("'");
    }
    if (tripStatus != null && tripStatus.trim().length() > 0) {
      sql.append(" AND TP.STATUS ='").append(tripStatus).append("'");
    }
    sql.append(" ORDER BY TP.MODIFIED_ON DESC LIMIT 20");
    System.out.println(sql);
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql.toString())) {
      stmt.setString(1, locationId);
      System.out.println("getTripList Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        TripBean trip = new TripBean();
        trip.setId(resultSet.getInt(1));
        trip.setTripName(resultSet.getString(2));
        trip.setTruckName(resultSet.getString(3));
        trip.setDriverName(resultSet.getString(4));
        trip.setDriverLicense(resultSet.getString(5));
        trip.setStartGateName(resultSet.getString(6));
        trip.setEndGateName(resultSet.getString(7));
        trip.setStartGateEntryTime(resultSet.getTimestamp(8));
        trip.setStartGateExitTime(resultSet.getTimestamp(9));
        trip.setEndGateEntryTime(resultSet.getTimestamp(10));
        trip.setEndGateExitTime(resultSet.getTimestamp(11));
        trip.setStatus(resultSet.getString(12));
        trip.setParkEntryTime(resultSet.getTimestamp(13));
        trip.setLocation(resultSet.getInt(14));
        tripList.add(trip);
        System.out.println("-----");
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return tripList;
  }

  public List<DistributorBean> getDistributorList(String distributorName, String locationId) {
    if (isEmpty(locationId)) {
      return null;
    }
    List<DistributorBean> distributorList = new ArrayList<DistributorBean>();
    StringBuilder sql =
        new StringBuilder("SELECT ID,NAME,SAP_CODE,DISTRIBUTOR_NUMBER,GODOWNKEEPER_NUMBER,LOCATION_ID,RTKM from " + dbName
            + ".DISTRIBUTOR WHERE IS_DELETED='N' AND LOCATION_ID=?");
    if (distributorName != null && distributorName.trim().length() > 0) {
      sql.append(" AND NAME LIKE '%").append(distributorName).append("%'");
    }
    sql.append(" ORDER BY MODIFIED_ON DESC LIMIT 20");
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql.toString())) {
      stmt.setString(1, locationId);
      System.out.println("getDistributorList Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        DistributorBean distributor = new DistributorBean();
        distributor.setId(resultSet.getInt(1));
        distributor.setName(resultSet.getString(2));
        distributor.setSapCode(resultSet.getString(3));
        distributor.setDistributorNumber(resultSet.getString(4));
        distributor.setGodownNumber(resultSet.getString(5));
        distributor.setLocation(resultSet.getInt(6));
        distributor.setRtkm(resultSet.getInt(7));
        distributorList.add(distributor);
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return distributorList;
  }


  public List<String> getAll(String table, String locationId) {
    if (isEmpty(locationId)) {
      return null;
    }
    List<String> nameList = new ArrayList<String>();
    StringBuilder sql = new StringBuilder();
    if (table.equals("TRUCK") || table.equals("DISTRIBUTOR") || table.equals("TRIP")) {
      sql.append("SELECT " + "NAME" + " from " + dbName + "." + table + " WHERE IS_DELETED='N' AND LOCATION_ID=?");
    }
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql.toString())) {
      stmt.setString(1, locationId);
      System.out.println("getAll Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        String name = resultSet.getString("NAME");
        nameList.add(name);
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return nameList;
  }

  public boolean editTruck(TruckBean truck) {
    int rows = 0;
    String sql =
        "UPDATE " + dbName + ".TRUCK SET DRIVER_NAME=?,DRIVER_NUMBER=?,DRIVER_ALT_NUMBER=?,DRIVER_LICENSE=?," +
        "CARRIER_CODE=?,CARRIER_NAME=? WHERE ID=?";
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setString(1, truck.getDriverName());
      stmt.setString(2, truck.getDriverNumber());
      stmt.setString(3, truck.getDriverAltNumber());
      stmt.setString(4, truck.getDriverLicense());
      stmt.setString(5, truck.getCarrierCode());
      stmt.setString(6, truck.getCarrierName());
      if (truck.getId() == 0)
        stmt.setString(7, null);
      else
        stmt.setInt(7, truck.getId());
      System.out.println("editTruck Query ----" + stmt);
      rows = stmt.executeUpdate();
      if (rows > 0) {
        return true;
      } else
        return false;
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean updateTrip(TripBean trip) {
    String sql = "UPDATE " + dbName + ".TRIP SET TRUCK_ID=?,END_GATE_ID=?,STATUS=?,"
        + "START_GATE_ENTRY_TIME=?,START_GATE_EXIT_TIME=?,END_GATE_ENTRY_TIME=?,END_GATE_EXIT_TIME=? WHERE ID=?";
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setInt(1, getTruckId(trip.getTruckName()));
      if (!(trip.getStatus().equals(TripStatus.LOADING.toString()))) {
        stmt.setInt(2, getGateId(trip.getEndGateName()));
      } else {
        stmt.setNull(2, java.sql.Types.INTEGER);
      }
      stmt.setString(3, trip.getStatus());
      if (trip.getStatus().equals(TripStatus.LOADING.toString())) {
        stmt.setTimestamp(4, new Timestamp(new Date().getTime()));
        stmt.setTimestamp(5, null);
        stmt.setTimestamp(6, null);
        stmt.setTimestamp(7, null);
      } else if (trip.getStatus().equals(TripStatus.ENROUTE.toString())) {
        stmt.setTimestamp(4, trip.getStartGateEntryTime());
        stmt.setTimestamp(5, new Timestamp(new Date().getTime()));
        stmt.setTimestamp(6, null);
        stmt.setTimestamp(7, null);
      } else if (trip.getStatus().equals(TripStatus.UNLOADING.toString())) {
        stmt.setTimestamp(4, trip.getStartGateEntryTime());
        stmt.setTimestamp(5, trip.getStartGateExitTime());
        stmt.setTimestamp(6, new Timestamp(new Date().getTime()));
        stmt.setTimestamp(7, null);
      } else if (trip.getStatus().equals(TripStatus.COMPLETED.toString())) {
        stmt.setTimestamp(4, trip.getStartGateEntryTime());
        stmt.setTimestamp(5, trip.getStartGateExitTime());
        stmt.setTimestamp(6, trip.getEndGateEntryTime());
        stmt.setTimestamp(7, new Timestamp(new Date().getTime()));
      }
      stmt.setInt(8, trip.getId());
      System.out.println("Update Trip Query----" + stmt);
      int rows = stmt.executeUpdate();
      if (rows > 0) {
        return true;
      } else
        return false;

    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  public boolean editTrip(TripBean trip) {
    String sql = "UPDATE " + dbName + ".TRIP SET END_GATE_ID=? WHERE ID=?";
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setInt(1, getGateId(trip.getEndGateName()));
      stmt.setInt(2, trip.getId());
      System.out.println("Edit Trip Query----" + stmt);
      int rows = stmt.executeUpdate();
      if (rows > 0) {
        return true;
      } else
        return false;

    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }
  
  public String editUser(UserBean user){
    int rows = 0;
    String response = "";
    String password = user.getPassword();
    String newPassword = getEncodedString(user.getNewPassword());
    if (password != null) {
      // Admin password change
      UserBean checkUser = getUser(user);
      String role = checkUser.getUserRole();
      if ((role == null) || (role.equals("")) || (role.length()==0)) {
        return "Incorrect Password";
      }
    }
    String sql = "UPDATE " + dbName + ".USER_LOGIN SET PASSWORD=?, MODIFIED_BY=? WHERE ID=?";
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setString(1, newPassword);
      stmt.setInt(2, user.getUserId());
      stmt.setInt(3, user.getId());
      System.out.println("editUser Query ----" + stmt);
      rows = stmt.executeUpdate();
      if (rows > 0) {
        response = "Details Updated";
      } else
        response = "Invalid Details";
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return response;
  }

  public boolean deleteObject(int id, String table) {
    int rows = 0;
    table = table.toUpperCase();
    if (table.equals("TRUCK") || table.equals("DISTRIBUTOR") || table.equals("TRIP")) {
      String sql = "UPDATE " + dbName + "." + table + " SET IS_DELETED='Y' WHERE ID=?";
      try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
        stmt.setInt(1, id);
        System.out.println("deleteObject Query ----" + stmt);
        rows = stmt.executeUpdate();
        if (rows > 0) {
          return true;
        } else
          return false;
      } catch (SQLException se) {
        se.printStackTrace();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    return false;
  }

  public boolean editDistributor(DistributorBean distributor) {
    String sql =
        "UPDATE " + dbName + ".DISTRIBUTOR SET SAP_CODE=?,RTKM=?,DISTRIBUTOR_NUMBER=?,GODOWNKEEPER_NUMBER=? WHERE ID=?";
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setString(1, distributor.getSapCode());
      stmt.setInt(2, distributor.getRtkm());
      stmt.setString(3, distributor.getDistributorNumber());
      stmt.setString(4, distributor.getGodownNumber());
      stmt.setInt(5, distributor.getId());
      System.out.println("editDistributor Query ----" + stmt);
      int rows = stmt.executeUpdate();
      if (rows > 0) {
        return true;
      } else
        return false;
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  private int getTruckId(String truckName) {
    int id = 0;
    String sql = "SELECT ID from " + dbName + ".TRUCK WHERE NAME=?";
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setString(1, truckName);
      System.out.println("getTruckId Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet.next()) {
        id = resultSet.getInt("ID");
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return id;
  }

  private int getGateId(String gateName) {
    int id = 0;
    String sql = "SELECT ID from " + dbName + ".DISTRIBUTOR WHERE NAME=?";
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setString(1, gateName);
      System.out.println("getGateId Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet.next()) {
        id = resultSet.getInt("ID");
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return id;
  }

  public void getReport(String from, String to, String truckName, String locationId) {
    StringBuilder sql = new StringBuilder("SELECT TK.NAME \"TRUCK NAME\",\r\n" + "TK.DRIVER_NAME \"DRIVER NAME\",\r\n"
        + "TK.DRIVER_NUMBER \"DRIVER NUMBER\",\r\n" + "TK.DRIVER_ALT_NUMBER \"DRIVER ALTERNATE NUMBER\",\r\n"
        + "TK.DRIVER_LICENSE \"DRIVER LICENSE\",\r\n" + "TR.PARKING_ENTRY_TIME \"PARKING TIME\",\r\n"
        + "TR.START_GATE_ENTRY_TIME \"START TIME GATE-A\", \r\n" + "TR.START_GATE_EXIT_TIME \"EXIT TIME GATE-A\", \r\n"
        + "TR.END_GATE_ENTRY_TIME \"START TIME GATE-B\", \r\n" + "TR.END_GATE_EXIT_TIME \"END TIME GATE-B\",\r\n"
        + "TR.STATUS \"STATUS\",\r\n"
        + "TIMEDIFF(TR.START_GATE_EXIT_TIME,TR.START_GATE_ENTRY_TIME) \"TIME AT GATE-A\",\r\n"
        + "TIMEDIFF(TR.END_GATE_EXIT_TIME,TR.END_GATE_ENTRY_TIME) \"TIME AT GATE-B\"\r\n" + "FROM truckdb.TRIP TR \r\n"
        + "LEFT JOIN truckdb.TRUCK TK ON TK.ID= TR.TRUCK_ID \r\n"
        + "WHERE TR.LOCATION_ID=? AND TR.PARKING_ENTRY_TIME BETWEEN ? AND ? AND TR.IS_DELETED = 'N'");
    if (truckName != null && truckName.trim().length() > 0)
      sql.append(" AND TK.NAME=?");
    Stream<String> lines = null;
    List<String> rows = new ArrayList<String>();
    rows.add(
        "TRUCK NAME,DRIVER NAME,DRIVER NUMBER,DRIVER ALTERNATE NUMBER,DRIVER LICENSE,PARKING TIME,START TIME GATE-A,EXIT TIME GATE-A,START TIME GATE-B,END TIME GATE-B,STATUS,TIME AT GATE-A,TIME AT GATE-B");
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql.toString())) {
      stmt.setString(1, locationId);
      stmt.setString(2, from);
      stmt.setString(3, to);
      if (truckName != null && truckName.trim().length() > 0)
        stmt.setString(4, truckName);
      System.out.println("getReport Query-------" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= 13; i++) {
          if (i == 13)
            result.append(resultSet.getString(i));
          else
            result.append(resultSet.getString(i) + ",");
        }
        rows.add(result.toString());
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    try {
      System.out.println("row---" + rows);
      lines = rows.stream();
      System.out.println("Row Size----" + rows.size());
      parse(lines, "truck-report.csv");
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public void getTruckRotation(String from, String to, String truckName, String locationId) {
    StringBuilder sql = new StringBuilder("SELECT TK.NAME \"TRUCK NAME\",\r\n" + "TK.DRIVER_NAME \"DRIVER NAME\",\r\n"
        + "TK.DRIVER_NUMBER \"DRIVER NUMBER\",\r\n" + "TK.DRIVER_LICENSE \"DRIVER LICENSE\",\r\n"
        + "TR.STATUS \"STATUS\",\r\n"
        + "TIMEDIFF(TR.START_GATE_ENTRY_TIME,TR.PARKING_ENTRY_TIME) \"TIME AT PARKING\",\r\n"
        + "TIMEDIFF(TR.START_GATE_EXIT_TIME,TR.START_GATE_ENTRY_TIME) \"TIME FOR LOADING\",\r\n"
        + "TIMEDIFF(TR.END_GATE_ENTRY_TIME,TR.START_GATE_EXIT_TIME) \"TIME FOR ENROUTE\",\r\n"
        + "TIMEDIFF(TR.END_GATE_EXIT_TIME,TR.END_GATE_ENTRY_TIME) \"TIME FOR UNLOADING\",\r\n"
        + "TIMEDIFF(TR.END_GATE_EXIT_TIME,TR.PARKING_ENTRY_TIME) \"TOTAL TIME\"\r\n" + "FROM truckdb.TRIP TR \r\n"
        + "LEFT JOIN truckdb.TRUCK TK ON TK.ID= TR.TRUCK_ID \r\n"
        + "WHERE TR.LOCATION_ID=? AND TR.PARKING_ENTRY_TIME BETWEEN ? AND ? AND TR.IS_DELETED = 'N'");
    if (truckName != null && truckName.trim().length() > 0)
      sql.append(" AND TK.NAME=?");
    Stream<String> lines = null;
    List<String> rows = new ArrayList<String>();
    rows.add(
        "TRUCK NAME,DRIVER NAME,DRIVER NUMBER,DRIVER LICENSE,STATUS,TIME AT PARKING,TIME FOR LOADING,TIME FOR ENROUTE,TIME FOR UNLOADING,TOTAL TIME");
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql.toString())) {
      stmt.setString(1, locationId);
      stmt.setString(2, from);
      stmt.setString(3, to);
      if (truckName != null && truckName.trim().length() > 0)
        stmt.setString(4, truckName);
      System.out.println("getRotationReport Query-------" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        StringBuilder result = new StringBuilder();
        for (int i = 1; i <= 10; i++) {
          if (i == 10)
            result.append(resultSet.getString(i));
          else
            result.append(resultSet.getString(i) + ",");
        }
        rows.add(result.toString());
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    try {
      System.out.println("row---" + rows);
      lines = rows.stream();
      System.out.println("Row Size----" + rows.size());
      parse(lines, "truck-rotation.csv");
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public List<ReportCardBean> getReportCard(String from, String to, String truckName, String locationId) {
    StringBuilder sql = new StringBuilder("SELECT @rownum:=@rownum+1 AS 'TRIP NO', " +
        " DATE_FORMAT(TR.START_GATE_EXIT_TIME,'%d/%b/%y') AS DATE, DI.NAME AS 'DISTRIBUTOR NAME', " +
        " DI.SAP_CODE AS 'DISTRIBUTOR CODE', DI.RTKM AS RTKM FROM TRIP TR, DISTRIBUTOR DI, TRUCK T, " +
        " (SELECT @rownum:=0) r WHERE TR.IS_DELETED = 'N' AND TR.STATUS IN ('ENROUTE', 'UNLOADING', 'COMPLETED') " +
        " AND TR.END_GATE_ID = DI.ID AND TR.START_GATE_EXIT_TIME BETWEEN ? AND ? AND TR.TRUCK_ID = T.ID " +
        " and T.NAME = ? AND TR.LOCATION_ID=?");
    List<ReportCardBean> reportCardBeanList = new ArrayList<ReportCardBean>();
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql.toString())) {
      stmt.setString(1, from);
      stmt.setString(2, to);
      stmt.setString(3, truckName);
      stmt.setString(4, locationId);
      System.out.println("getReportCard Query-------" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        ReportCardBean reportCardBean = new ReportCardBean();
        reportCardBean.setTripNo(resultSet.getInt(1));
        reportCardBean.setDate(resultSet.getString(2));
        reportCardBean.setDistributorName(resultSet.getString(3));
        reportCardBean.setDistributorCode(resultSet.getString(4));
        reportCardBean.setRtkm(resultSet.getInt(5));
        reportCardBeanList.add(reportCardBean);
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return reportCardBeanList;
  }

  public String getLocation(String locationId){
    String locationName = null;
    String sql = "SELECT NAME from " + dbName + ".LOCATION WHERE ID=?";
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      stmt.setString(1, locationId);
      System.out.println("getLocationName Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      if (resultSet.next()) {
        locationName = resultSet.getString("NAME");
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return locationName;
  }

  public List<String> getAnnouncements(){
    List<String> announcements = new ArrayList<>();
    String sql = "SELECT ANNOUNCE from " + dbName + ".ANNOUNCEMENT WHERE IS_DELETED='N'";
    try (Connection conn = ormUtils.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
      System.out.println("getAnnouncement Query ----" + stmt);
      ResultSet resultSet = stmt.executeQuery();
      while (resultSet.next()) {
        announcements.add(resultSet.getString("ANNOUNCE"));
      }
    } catch (SQLException se) {
      se.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return announcements;
  }

  private void parse(Stream<String> lines, String output) throws IOException {
    final FileWriter fw = new FileWriter(output);
    // @formatter:off
    lines.forEach(data -> writeToFile(fw, data));
    // @formatter:on
    fw.close();
    lines.close();
  }

  private void writeToFile(FileWriter fw, String data) {
    try {
      fw.write(String.format("%s%n", data));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private boolean isEmpty(String input) {
    if (input != null && input.trim().length() > 0)
      return false;
    else
      return true;
  }

  public static void main(String[] args) {
    System.out.println(new Orm().getAnnouncements());
  }
}
