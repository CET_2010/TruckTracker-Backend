package com.truck.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.google.zxing.EncodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.sun.jersey.multipart.FormDataParam;
import com.truck.bean.DistributorBean;
import com.truck.bean.TripBean;
import com.truck.bean.TruckBean;
import com.truck.bean.UserBean;
import com.truck.util.QRCodeReaderWriter;

@Path("/truck/service")
@Produces("application/json")
@Consumes("application/json")
public class TruckService {

  private Orm orm = new Orm();

  @GET
  @Path("/get")
  public UserBean getUserTest() {
    UserBean user = new UserBean();
    user.setUsername("Nsr");
    user.setPassword("p@12");
    return user;
  }

  @GET
  @Path("/get2")
  public Response getUserTest2() {
    UserBean user = new UserBean();
    user.setUsername("Response op");
    user.setPassword("p@12");
    return Response.status(201).entity(user).build();
  }

  @GET
  @Path("/getAll/{table}")
  public Response getAll(@PathParam("table") String table, @PathParam("locationId") String locationId) {
    List<String> names = orm.getAll(table, locationId);
    return Response.status(201).entity(names).build();
  }

  @POST
  @Path("/userLogin")
  public Response userLogin(UserBean user) {
    UserBean verfiedUser = orm.getUser(user);
    return Response.status(200).entity(verfiedUser).build();
  }

  @POST
  @Path("/addUser")
  public Response addUser(UserBean user) {
    String response = orm.addUser(user);
    return Response.status(201).entity(response).build();
  }

  @POST
  @Path("/addTruck")
  public Response addTruck(TruckBean truck) {
    String response = orm.addTruck(truck);
    return Response.status(201).entity(response).build();
  }

  @POST
  @Path("/addGate")
  public Response addGate(DistributorBean distributor) {
    String response = orm.addDistributor(distributor);
    return Response.status(201).entity(response).build();
  }

  @POST
  @Path("/addTrip")
  public Response addTrip(TripBean trip) {
    String response = orm.addTrip(trip);
    return Response.status(201).entity(response).build();
  }

  @GET
  @Path("/getUserList")
  public Response getUserList(@QueryParam("userName") String userName, @QueryParam("locationId") String locationId) {
    return Response.status(200).entity(orm.getUserList(userName, locationId)).build();
  }

  @GET
  @Path("/getTruckList")
  public Response getTruckList(@QueryParam("truckName") String truckName,
      @QueryParam("distributorId") String distributorId, @QueryParam("locationId") String locationId) {
    return Response.status(200).entity(orm.getTruckList(truckName, distributorId, locationId)).build();
  }

  @GET
  @Path("/getDistributorList")
  public Response getDistributorList(@QueryParam("distributorName") String distributorName,
      @QueryParam("locationId") String locationId) {
    return Response.status(200).entity(orm.getDistributorList(distributorName, locationId)).build();
  }

  @GET
  @Path("/getTripList")
  public Response getTripList(@QueryParam("tripName") String tripName,
      @QueryParam("distributionId") String distributionId, @QueryParam("locationId") String locationId,
      @QueryParam("tripStatus") String tripStatus) {
    return Response.status(200).entity(orm.getTripList(tripName, distributionId, locationId, tripStatus)).build();
  }

  @GET
  @Path("/getTripCount")
  public Response getTripCount(@QueryParam("locationId") String locationId,
      @QueryParam("distributorId") String distributorId) {
    return Response.status(200).entity(orm.getTripCount(locationId, distributorId)).build();
  }

  @POST
  @Path("/editTruck")
  public Response editTruck(TruckBean truck) {
    return Response.status(200).entity(orm.editTruck(truck)).build();
  }

  @POST
  @Path("/editDistributor")
  public Response editDistributor(DistributorBean distributor) {
    return Response.status(200).entity(orm.editDistributor(distributor)).build();
  }

  @POST
  @Path("/updateTrip")
  public Response updateTrip(TripBean trip) {
    return Response.status(200).entity(orm.updateTrip(trip)).build();
  }

  @POST
  @Path("/editTrip")
  public Response editTrip(TripBean trip) {
    return Response.status(200).entity(orm.editTrip(trip)).build();
  }

  @POST
  @Path("/editUser")
  public Response editUser(UserBean user) {
    return Response.status(200).entity(orm.editUser(user)).build();
  }

  @POST
  @Path("/add")
  public Response addObjects(Object object) {
    String isAdded = null;
    if (object instanceof TruckBean) {
      TruckBean truck = (TruckBean) object;
      isAdded = orm.addTruck(truck);
    } else if (object instanceof TripBean) {
      TripBean trip = (TripBean) object;
      isAdded = orm.addTrip(trip);
    } else if (object instanceof DistributorBean) {
      DistributorBean distributor = (DistributorBean) object;
      isAdded = orm.addDistributor(distributor);
    }

    return Response.status(201).entity("Rows Added" + isAdded).build();

  }

  @POST
  @Path("/edit/{id}")
  public Response editObjects(@PathParam("id") int id, Object object) {
    boolean isAdded = false;
    if (object instanceof TruckBean) {
      TruckBean truck = (TruckBean) object;
      isAdded = orm.editTruck(truck);
    } else if (object instanceof TripBean) {
      TripBean trip = (TripBean) object;
      isAdded = orm.editTrip(trip);
    } else if (object instanceof DistributorBean) {
      DistributorBean distributor = (DistributorBean) object;
      isAdded = orm.editDistributor(distributor);
    }

    return Response.status(201).entity("Rows Edited" + isAdded).build();
  }

  @GET
  @Path("/delete/{id}/{table}")
  public Response editObjects(@PathParam("id") int id, @PathParam("table") String table) {
    boolean isDeleted = orm.deleteObject(id, table);
    return Response.status(201).entity(isDeleted).build();
  }

  @GET
  @Path("/getReport")
  @Produces("text/plain")
  public Response getReport(@QueryParam("from") String from, @QueryParam("to") String to,
      @QueryParam("truckName") String truckName, @QueryParam("locationId") String locationId) {
    // Create The report and save the File in a location in the server
    orm.getReport(from, to, truckName, locationId);
    File file = new File("truck-report.csv");
    DateFormat formatter = new SimpleDateFormat("dd-MMM-yy_HH:mm");
    Date date = new Date();
    String formattedDate = formatter.format(date);
    ResponseBuilder response = Response.ok((Object) file);
    response.header("Content-Disposition", "attachment; filename=\"truck-report-" + formattedDate + ".csv\"");
    return response.build();
  }

  @GET
  @Path("/getTruckRotation")
  @Produces("text/plain")
  public Response getTruckRotation(@QueryParam("from") String from, @QueryParam("to") String to,
      @QueryParam("truckName") String truckName, @QueryParam("locationId") String locationId) {
    // Create The report and save the File in a location in the server
    orm.getTruckRotation(from, to, truckName, locationId);
    File file = new File("truck-rotation.csv");
    DateFormat formatter = new SimpleDateFormat("dd-MMM-yy_HH:mm");
    Date date = new Date();
    String formattedDate = formatter.format(date);
    ResponseBuilder response = Response.ok((Object) file);
    response.header("Content-Disposition", "attachment; filename=\"truck-rotation-" + formattedDate + ".csv\"");
    return response.build();
  }

  @GET
  @Path("/getReportCard")
  public Response getReportCard(@QueryParam("from") String from, @QueryParam("to") String to,
      @QueryParam("truckName") String truckName, @QueryParam("locationId") String locationId) {
    return Response.status(200).entity(orm.getReportCard(from, to, truckName, locationId)).build();
  }

  @GET
  @Path("/getLocationName")
  @Produces("text/plain")
  public Response getLocation(@QueryParam("locationId") String locationId) {
    return Response.status(200).entity(orm.getLocation(locationId)).build();
  }

  @GET
  @Path("/getAnnouncements")
  public Response getAnnouncements() {
    return Response.status(200).entity(orm.getAnnouncements()).build();
  }


  @POST
  @Path("/getQRCode")
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  public Response getQRCode(@FormDataParam("file") FileInputStream file) {
    String qrCode = null;
    try {
      qrCode = QRCodeReaderWriter.readQRCode(file);
    } catch (NotFoundException | IOException e) {
      e.printStackTrace();
    }
    return Response.status(200).entity(qrCode).build();
  }


  @GET
  @Path("/getQRCode")
  @Produces("image/png")
  public Response getQRCode(@QueryParam("text") String message) {
    String filePath = message + "_QRCode.png";
    String charset = "UTF-8"; // or "ISO-8859-1"
    Map hintMap = new HashMap();
    hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
    try {
      QRCodeReaderWriter.createQRCode(message, filePath, charset, hintMap, 200, 200);
    } catch (WriterException | IOException e) {
      e.printStackTrace();
    }
    File qrImage = new File(filePath);
    ResponseBuilder response = Response.ok((Object) qrImage);
    response.header("Content-Disposition", "attachment; filename=" + filePath);
    response.header("Content-Type", "image/png");
    return response.build();
  }

  public static void main(String[] args) {
    System.out.println(new TruckService().getQRCode("Nisar"));
  }
}
