package com.truck.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DistributorBean {
  private int id;
  private String name;
  private int userId;
  private String sapCode;
  private int rtkm;
  private String distributorNumber;
  private String godownNumber;
  private int location;

  public int getLocation() {
    return location;
  }

  public void setLocation(int location) {
    this.location = location;
  }

  public DistributorBean() {
    super();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getSapCode() {
    return sapCode;
  }

  public void setSapCode(String sapCode) {
    this.sapCode = sapCode;
  }

  public int getRtkm() {
    return rtkm;
  }

  public void setRtkm(int rtkm) {
    this.rtkm = rtkm;
  }

  public String getDistributorNumber() {
    return distributorNumber;
  }

  public void setDistributorNumber(String distributorNumber) {
    this.distributorNumber = distributorNumber;
  }

  public String getGodownNumber() {
    return godownNumber;
  }

  public void setGodownNumber(String godownNumber) {
    this.godownNumber = godownNumber;
  }

  public DistributorBean(int id, String name, int userId, String sapCode, int rtkm, 
      String distributorNumber, String godownNumber, int location) {
    super();
    this.id = id;
    this.name = name;
    this.userId = userId;
    this.sapCode = sapCode;
    this.rtkm = rtkm;
    this.distributorNumber = distributorNumber;
    this.godownNumber = godownNumber;
    this.location = location;
  }

  @Override
  public String toString() {
    return "DistributorBean [id=" + id + ", name=" + name + ", userId=" + userId + ", sapCode=" + sapCode
        + ", rtkm=" + rtkm + ", distributorNumber=" + distributorNumber + ", godownNumber=" + godownNumber 
        + ", location=" + location + "]";
  }

}
