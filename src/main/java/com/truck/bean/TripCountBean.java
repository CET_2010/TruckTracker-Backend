package com.truck.bean;

public class TripCountBean {
  private int park_count, load_count, unload_count, enroute_count, complete_count;

  public TripCountBean(int park_count, int load_count, int unload_count, int enroute_count, int complete_count) {
    super();
    this.park_count = park_count;
    this.load_count = load_count;
    this.unload_count = unload_count;
    this.enroute_count = enroute_count;
    this.complete_count = complete_count;
  }
  public TripCountBean() {
    super();
  }

  public int getPark_count() {
    return park_count;
  }

  public void setPark_count(int park_count) {
    this.park_count = park_count;
  }

  public int getLoad_count() {
    return load_count;
  }

  public void setLoad_count(int load_count) {
    this.load_count = load_count;
  }

  public int getUnload_count() {
    return unload_count;
  }

  public void setUnload_count(int unload_count) {
    this.unload_count = unload_count;
  }

  public int getEnroute_count() {
    return enroute_count;
  }

  public void setEnroute_count(int enroute_count) {
    this.enroute_count = enroute_count;
  }

  public int getComplete_count() {
    return complete_count;
  }

  public void setComplete_count(int complete_count) {
    this.complete_count = complete_count;
  }

  @Override
  public String toString() {
    return "TripCountBean [park_count=" + park_count + ", load_count=" + load_count + ", unload_count=" + unload_count
        + ", enroute_count=" + enroute_count + ", complete_count=" + complete_count + "]";
  }
}
