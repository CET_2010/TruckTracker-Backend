package com.truck.bean;

import java.sql.Timestamp;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TripBean {
  private int id;
  private String tripName;
  private String truckName;
  private String driverName;
  private String driverLicense;
  private String startGateName;
  private Timestamp parkEntryTime;
  private Timestamp startGateEntryTime;
  private Timestamp startGateExitTime;
  private String endGateName;
  private Timestamp endGateEntryTime;
  private Timestamp endGateExitTime;
  private String status;
  private int userId;
  private int location;

  public TripBean() {
    super();
  }

  public TripBean(String tripName, String truckName, String driverName, String driverLicense, String startGateName,
      Timestamp parkEntryTime, Timestamp startGateEntryTime, Timestamp startGateExitTime, String endGateName,
      Timestamp endGateEntryTime, Timestamp endGateExitTime, String status, int userId, int id, int location) {
    super();
    this.tripName = tripName;
    this.truckName = truckName;
    this.driverName = driverName;
    this.driverLicense = driverLicense;
    this.startGateName = startGateName;
    this.parkEntryTime = parkEntryTime;
    this.startGateEntryTime = startGateEntryTime;
    this.startGateExitTime = startGateExitTime;
    this.endGateName = endGateName;
    this.endGateEntryTime = endGateEntryTime;
    this.endGateExitTime = endGateExitTime;
    this.status = status;
    this.userId = userId;
    this.id = id;
    this.location = location;
  }

  public int getLocation() {
    return location;
  }

  public void setLocation(int location) {
    this.location = location;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTripName() {
    return tripName;
  }

  public void setTripName(String tripName) {
    this.tripName = tripName;
  }

  public String getTruckName() {
    return truckName;
  }

  public void setTruckName(String truckName) {
    this.truckName = truckName;
  }

  public String getDriverName() {
    return driverName;
  }

  public void setDriverName(String driverName) {
    this.driverName = driverName;
  }

  public String getDriverLicense() {
    return driverLicense;
  }

  public void setDriverLicense(String driverLicense) {
    this.driverLicense = driverLicense;
  }

  public String getStartGateName() {
    return startGateName;
  }

  public void setStartGateName(String startGateName) {
    this.startGateName = startGateName;
  }

  @JsonSerialize(using = JSONDateSerializer.class)
  public Timestamp getStartGateEntryTime() {
    return startGateEntryTime;
  }

  public void setStartGateEntryTime(Timestamp startGateEntryTime) {
    this.startGateEntryTime = startGateEntryTime;
  }

  @JsonSerialize(using = JSONDateSerializer.class)
  public Timestamp getStartGateExitTime() {
    return startGateExitTime;
  }

  public void setStartGateExitTime(Timestamp startGateExitTime) {
    this.startGateExitTime = startGateExitTime;
  }

  public String getEndGateName() {
    return endGateName;
  }

  public void setEndGateName(String endGateName) {
    this.endGateName = endGateName;
  }

  @JsonSerialize(using = JSONDateSerializer.class)
  public Timestamp getEndGateEntryTime() {
    return endGateEntryTime;
  }

  public void setEndGateEntryTime(Timestamp endGateEntryTime) {
    this.endGateEntryTime = endGateEntryTime;
  }

  @JsonSerialize(using = JSONDateSerializer.class)
  public Timestamp getEndGateExitTime() {
    return endGateExitTime;
  }

  public void setEndGateExitTime(Timestamp endGateExitTime) {
    this.endGateExitTime = endGateExitTime;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  @JsonSerialize(using = JSONDateSerializer.class)
  public Timestamp getParkEntryTime() {
    return parkEntryTime;
  }


  public void setParkEntryTime(Timestamp parkEntryTime) {
    this.parkEntryTime = parkEntryTime;
  }

  @Override
  public String toString() {
    return "TripBean [id=" + id + ", tripName=" + tripName + ", truckName=" + truckName + ", driverName=" + driverName
        + ", driverLicense=" + driverLicense + ", startGateName=" + startGateName + ", parkEntryTime=" + parkEntryTime
        + ", startGateEntryTime=" + startGateEntryTime + ", startGateExitTime=" + startGateExitTime + ", endGateName="
        + endGateName + ", endGateEntryTime=" + endGateEntryTime + ", endGateExitTime=" + endGateExitTime + ", status="
        + status + ", userId=" + userId + ", location=" + location + "]";
  }

}
