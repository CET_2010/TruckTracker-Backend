package com.truck.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TruckBean {

  private int id;
  private String truckName;
  private String driverName;
  private String driverNumber;
  private String driverAltNumber;
  private String driverLicense;
  private String carrierCode;
  private String carrierName;
  private int userId;
  private int location;

  public TruckBean() {
    super();
  } 

  public int getLocation() {
    return location;
  }

  public void setLocation(int location) {
    this.location = location;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTruckName() {
    return truckName;
  }

  public void setTruckName(String truckName) {
    this.truckName = truckName;
  }

  public String getDriverName() {
    return driverName;
  }

  public void setDriverName(String driverName) {
    this.driverName = driverName;
  }

  public String getDriverNumber() {
    return driverNumber;
  }

  public void setDriverNumber(String driverNumber) {
    this.driverNumber = driverNumber;
  }

  public String getDriverAltNumber() {
    return driverAltNumber;
  }

  public void setDriverAltNumber(String driverAltNumber) {
    this.driverAltNumber = driverAltNumber;
  }

  public String getDriverLicense() {
    return driverLicense;
  }

  public void setDriverLicense(String driverLicense) {
    this.driverLicense = driverLicense;
  }

  public String getCarrierCode() {
    return carrierCode;
  }

  public void setCarrierCode(String carrierCode) {
    this.carrierCode = carrierCode;
  }

  public String getCarrierName() {
    return carrierName;
  }

  public void setCarrierName(String carrierName) {
    this.carrierName = carrierName;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public TruckBean(int id, String truckName, String driverName, String driverNumber, String driverAltNumber,
      String driverLicense, String carrierCode, String carrierName, int userId, int location) {
    super();
    this.id = id;
    this.truckName = truckName;
    this.driverName = driverName;
    this.driverNumber = driverNumber;
    this.driverAltNumber = driverAltNumber;
    this.driverLicense = driverLicense;
    this.carrierCode = carrierCode;
    this.carrierName = carrierName;
    this.userId = userId;
    this.location = location;
  }

  @Override
  public String toString() {
    return "TruckBean [id=" + id + ", truckName=" + truckName + ", driverName=" + driverName + ", driverNumber="
        + driverNumber + ", driverAltNumber=" + driverAltNumber + ", driverLicense=" + driverLicense + ", carrierCode="
        + carrierCode + ", carrierName=" + carrierName + ", userId=" + userId + ", location=" + location + "]";
  }

}
