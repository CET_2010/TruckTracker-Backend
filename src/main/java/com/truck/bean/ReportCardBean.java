package com.truck.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReportCardBean {
  private int tripNo;
  private String date;
  private String distributorName;
  private String distributorCode;
  private int rtkm;

  public ReportCardBean() {
    super();
  }
  
  public ReportCardBean(int tripNo, String date, String distributorName, String distributorCode, int rtkm) {
    this.tripNo = tripNo;
    this.date = date;
    this.distributorName = distributorName;
    this.distributorCode = distributorCode;
    this.rtkm = rtkm;
  }

  public int getTripNo() {
    return tripNo;
  }

  public void setTripNo(int tripNo) {
    this.tripNo = tripNo;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getDistributorName() {
    return distributorName;
  }

  public void setDistributorName(String distributorName) {
    this.distributorName = distributorName;
  }

  public String getDistributorCode() {
    return distributorCode;
  }

  public void setDistributorCode(String distributorCode) {
    this.distributorCode = distributorCode;
  }

  public int getRtkm() {
    return rtkm;
  }

  public void setRtkm(int rtkm) {
    this.rtkm = rtkm;
  }

  @Override
  public String toString() {
    return "ReportCardBean [tripNo=" + tripNo + ", date=" + date + ", distributorName=" + distributorName
        + ", distributorCode=" + distributorCode + ", rtkm=" + rtkm + "]";
  }
}
