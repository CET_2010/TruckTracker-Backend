DELIMITER $$
CREATE PROCEDURE truckdb.sp_createUser(
            IN p_user_name VARCHAR(30),
            IN p_password VARCHAR(50),
            IN p_role VARCHAR(20),
            IN p_distributor_id INT(10),
            IN p_user_Id INT(10),
            IN p_location_Id INT(10),
            OUT p_res VARCHAR(20)
            )
BEGIN
	IF ( SELECT EXISTS (SELECT 1 FROM truckdb.USER_LOGIN WHERE NAME = p_user_name AND LOCATION_ID = p_location_Id) ) THEN
		SELECT 'Username Exists' INTO p_res;
	ELSE
    	INSERT INTO truckdb.USER_LOGIN (NAME, PASSWORD, ROLE, DISTRIBUTOR_ID, CREATED_BY, LOCATION_ID) 
    	VALUES (p_user_name, p_password, p_role, p_distributor_id, p_user_Id, p_location_Id);
	END IF;
END;$$ 

CREATE PROCEDURE truckdb.sp_createTruck(
            IN p_name VARCHAR(30),
            IN p_driver_name VARCHAR(30),
            IN p_driver_number VARCHAR(30),
            IN p_driver_alt_number VARCHAR(30),
            IN p_driver_license VARCHAR(30),
            IN p_carrier_code VARCHAR(10),
            IN p_carrier_name VARCHAR(30),
            IN p_user_Id INT(10),
            IN p_location_Id INT(10),
            OUT p_res VARCHAR(20)
            )
BEGIN
	IF ( SELECT EXISTS (SELECT 1 FROM truckdb.TRUCK WHERE NAME = p_name AND LOCATION_ID = p_location_Id) ) THEN
		SELECT 'Truck Exists' INTO p_res;
	ELSE
    	INSERT INTO truckdb.TRUCK (NAME, DRIVER_NAME, DRIVER_NUMBER, DRIVER_ALT_NUMBER, DRIVER_LICENSE,
    	CARRIER_CODE, CARRIER_NAME, CREATED_BY, LOCATION_ID) VALUES (p_name, p_driver_name, p_driver_number, 
    	p_driver_alt_number, p_driver_license, p_carrier_code, p_carrier_name, p_user_Id, p_location_Id);
	END IF;
END;$$ 

CREATE PROCEDURE truckdb.sp_createDistributor(
            IN p_name VARCHAR(30),
            IN p_sap_code VARCHAR(10),
            IN p_rtkm INT(10),
            IN p_dist_number VARCHAR(30),
            IN p_godown_number VARCHAR(30),
            IN p_user_Id INT(10),
            IN p_location_Id INT(10),
            OUT p_res VARCHAR(20)
            )
BEGIN
	IF ( SELECT EXISTS (SELECT 1 FROM truckdb.DISTRIBUTOR WHERE SAP_CODE = p_sap_code AND LOCATION_ID = p_location_Id) ) THEN
		SELECT 'Gate Exists' INTO p_res;
	ELSE
    	INSERT INTO truckdb.DISTRIBUTOR (NAME, SAP_CODE, RTKM, DISTRIBUTOR_NUMBER, GODOWNKEEPER_NUMBER, CREATED_BY, LOCATION_ID) 
    	VALUES (p_name, p_sap_code, p_rtkm, p_dist_number, p_godown_number, p_user_Id, p_location_Id);
	END IF;
END;$$ 

CREATE PROCEDURE truckdb.sp_createTrip(
            IN p_name VARCHAR(30),
            IN p_user_Id INT(10),
            IN p_location_Id INT(10),
            OUT p_res VARCHAR(20)
            )
BEGIN
	DECLARE trip_name VARCHAR(30);
	SELECT CONCAT(p_name, '_', (SELECT DATE_FORMAT(NOW(), '%b-%d-%y_%H:%i') FROM DUAL)) INTO trip_name FROM DUAL;
	IF ( SELECT EXISTS (SELECT 1 FROM truckdb.TRIP WHERE NAME = trip_name) ) THEN
		SELECT 'Trip Exists' INTO p_res;
	ELSE
		INSERT INTO truckdb.TRIP (NAME, TRUCK_ID, CREATED_BY, PARKING_ENTRY_TIME, START_GATE_ENTRY_TIME, LOCATION_ID, STATUS)
    	VALUES (trip_name, (SELECT ID FROM truckdb.TRUCK WHERE NAME = p_name), 
		p_user_Id, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, p_location_Id, 'LOADING');
	END IF;
END;$$ 
DELIMITER ;